# Prueba moveapps
## Ejecución

- Importar proyecto a Eclipse spring boot

## Ejecución
- Postman 
    Servicio POST: http://localhost:8080/api-usuario/v2/authentication
    Detalle: Generación de Token Bearer para invocar a los otros servicios.
    Body:
    {
        "pass": "moveapps",
        "user": "moveapps"
    }

    Servicio POST: http://localhost:8080/api-usuario/v2/user/registro
    Ejecución: Token generado por servicio de authentication pasar por postman opción
    Headers, agregar atributo Authorization valor "Bearer [token]".
    Body:
    {
        "email": "pepe@gmail.com",
        "password": "Abcdas11#",
        "phones": [
            {
                "number": "12345678",
                "cityCode": "1",
                "contryCode": "2"
            },
            {
                "number": "87654321",
                "cityCode": "3",
                "contryCode": "4"
            }
        ]
    }

    Servicio GET: http://localhost:8080/api-usuario/v2/user/lista
    Ejecución: Token generado por servicio de authentication pasar por postman opción
    Headers, agregar atributo Authorization valor "Bearer [token]".