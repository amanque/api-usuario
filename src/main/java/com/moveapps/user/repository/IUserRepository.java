package com.moveapps.user.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.moveapps.user.model.User;


@Repository
public interface IUserRepository extends JpaRepository<User, UUID>{
	
	@Query(value = "select * from usuario WHERE active = ?1",  nativeQuery = true)
	public List<User> listActive(boolean isActive);
	
	public User findByEmailAndPassword(String email, String password);
	
	public User findByEmail(String email);
		
	public List<User> findByActive(boolean isActive);
	
	
}
