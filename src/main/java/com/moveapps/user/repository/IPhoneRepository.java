package com.moveapps.user.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.moveapps.user.model.Phone;

@Repository
public interface IPhoneRepository extends JpaRepository<Phone, UUID>{
	
}
