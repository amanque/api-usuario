package com.moveapps.user.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(of = { "idPhone", "number", "cityCode", "contryCode"})
@Entity
@Table(name = "phone")
public class Phone implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "idphone")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idPhone;
	
	@Column(nullable = false, name = "number")
	private String number;
	
	@Column(nullable = false, name = "citycode")
	private String cityCode;
	
	@Column(nullable = false, name = "contrycode")
	private String contryCode;
	
	@Column(nullable = false, name = "iduser_rel")
	private UUID iduser_rel;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "iduser_rel", insertable = false, updatable = false, nullable = false)
	private User userPhone;

}
