package com.moveapps.user.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.moveapps.user.audit.Auditable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "usuario")
@ToString(of = { "idUser", "email", "password", "token", "active" })
public class User extends Auditable implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Type(type = "pg-uuid")
	@GenericGenerator(name = "uuid-gen", strategy = "uuid2")
	@GeneratedValue(generator = "uuid-gen")
	@Column(name = "iduser")
	@Getter
	@Setter
	private UUID idUser;

	@Column(nullable = false, name = "email")
	@Getter
	@Setter
	private String email;

	@Column(nullable = false, name = "password")
	@Getter
	@Setter
	private String password;

	@Column(nullable = true, name = "token")
	@Getter
	@Setter
	private String token;

	@Column(nullable = false, name = "active")
	@Getter
	@Setter
	private boolean active;

	@OneToMany(mappedBy = "userPhone")
	@Getter
	@Setter
	@JsonIgnore
	private List<Phone> phones = new ArrayList<Phone>();
}
