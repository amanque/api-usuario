package com.moveapps.user.audit;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Auditable {

	@CreatedDate
	@Temporal(TIMESTAMP)
	@Column(nullable = false, name = "created")
	@Getter
	@Setter
	private Date created;

	@LastModifiedDate
	@Temporal(TIMESTAMP)
	@Column(nullable = true, name = "modified")
	@Getter
	@Setter
	private Date modified;

	@Column(nullable = false, name = "last_login")
	@Getter
	private Date last_login;

	public void setLast_login(Date last_login) {
		if (getModified() != null)
			this.last_login = getModified();
		else
			this.last_login = getCreated();
	}

}
