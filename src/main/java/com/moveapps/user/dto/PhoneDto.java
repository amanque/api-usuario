package com.moveapps.user.dto;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.googlecode.jmapper.annotations.JMap;

import lombok.Data;

@Data
public class PhoneDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@JMap
	private String number;
	@JMap
	private String cityCode;
	@JMap
	private String contryCode;
	@JsonIgnore
	@JMap
	private UUID iduser_rel;
	
}
