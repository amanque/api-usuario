package com.moveapps.user.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class TokenDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private final String token;

}
