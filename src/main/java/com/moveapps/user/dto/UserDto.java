package com.moveapps.user.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.googlecode.jmapper.annotations.JMap;

import lombok.Data;

@Data
public class UserDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@JMap
	private UUID idUser;
	@JMap
	private String email;
	@JMap
	private String password;
	@JMap
	private Date created;
	@JMap
	private Date modified;
	@JMap
	private Date last_login;
	@JMap
	private String token;
	@JMap
	private boolean active;
	@JMap
	private List<PhoneDto> phones;
	
}
