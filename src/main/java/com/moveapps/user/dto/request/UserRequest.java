package com.moveapps.user.dto.request;

import java.util.List;

import lombok.Data;

@Data
public class UserRequest {
	private String email;
	private String password;
	private List<PhoneRequest> phones;
}
