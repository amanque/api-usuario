package com.moveapps.user.utils;

public class Constants {

	public static final String CODIGO_REGLA_EXITOSA = "001";
	public static final String CODIGO_REGLA_SIN_DATOS = "002";
	public static final String CODIGO_REGLA_ERROR_API = "003";

	public static final String MENSAJE_ERROR_API = "Error en el servicio api rest ";
	public static final String MENSAJE_REGLA_EXITOSA = "Regla aplicada correctamente";
	public static final String MENSAJE_REGLA_NO_EXITOSA = "Regla no aplicada";

	public static final String SEPARADOR_SLASH = "/";
	public static final int STATUS_OK = 200;
	public static final int UNAUTHORIZED = 401;
	public static final String AUTHORIZATION = "Authorization";

	public static final String ERROR_TOKEN = "Token no es válido";
	public static final String ERROR_FORMAT_EMAIL = "Formato de correo electrónico inválido.";
	public static final String ERROR_FORMAT_PASS = "Formato de password inválido, debe contener una Mayúscula, letras minúsculas, y dos números.";
	public static final String ERROR_EMAIL_EXIST = "El correo ya registrado.";
	public static final String ERROR_LIST_USERS = "No existen usuario para Listar.";
	public static final String ERROR_USER_TOKEN = "Datos de usuario y password incorrectos.";
	
    public static final String SIGN_UP_URL = "/user/registro";
    public static final boolean USERS_ACTIVE = true;
	public static final String BEARER = "Bearer ";
	public static final int LOWER_CASE = 0;
	public static final int CAPITAL_LETTER = 1;
	public static final int NUMBERS = 2;
	public static final int EQUALS = 0;
	public static final int MAYOR = 1;
}
