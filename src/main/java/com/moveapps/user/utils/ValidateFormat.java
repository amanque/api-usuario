package com.moveapps.user.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateFormat {
	
	public static boolean valExpresionReg(String expReg, String text) {
		Pattern VALID_REGEX = Pattern.compile(expReg, Pattern.CASE_INSENSITIVE);
		Matcher matcher = VALID_REGEX.matcher(text);
        return matcher.find();
	}
	
	public static boolean valExpresionReg(String expReg, String text, int countChar, int operator) {
		Pattern VALID_REGEX = Pattern.compile(expReg, Pattern.DOTALL);
		Matcher matcher = VALID_REGEX.matcher(text);		
		int count = 0;
		while (matcher.find()) {
		    count++;
		}
		
		if(count == countChar && operator == Constants.EQUALS)
			return true;
		
		if(count > countChar && operator == Constants.MAYOR)
			return true;
		
        return false;
	}

}
