package com.moveapps.user.service;

import java.util.List;
import java.util.UUID;

import com.moveapps.user.dto.request.PhoneRequest;

public interface IPhoneService {
	public void getPhoneBean(List<PhoneRequest> list, UUID idUser);

}
