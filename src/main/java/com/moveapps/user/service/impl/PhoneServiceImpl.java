package com.moveapps.user.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.moveapps.user.dto.PhoneDto;
import com.moveapps.user.dto.request.PhoneRequest;
import com.moveapps.user.model.Phone;
import com.moveapps.user.repository.IPhoneRepository;
import com.moveapps.user.service.IPhoneService;
import com.moveapps.user.utils.Mapper;

@Service("PhoneService")
public class PhoneServiceImpl implements IPhoneService {
	
	@Autowired
	public IPhoneRepository phoneRepository;

	@Override
	public void getPhoneBean(List<PhoneRequest> phoneRequest, UUID idUser) {
		List<PhoneDto> listPhoneDto = new ArrayList<PhoneDto>();
		for (PhoneRequest phone : phoneRequest) {
			PhoneDto phoneDto = new PhoneDto();
			phoneDto.setCityCode(phone.getCityCode());
			phoneDto.setContryCode(phone.getContryCode());
			phoneDto.setCityCode(phone.getCityCode());
			phoneDto.setIduser_rel(idUser);
			phoneDto.setNumber(phone.getNumber());
			listPhoneDto.add(phoneDto);
		}
		
		List<Phone> phones = Mapper.mapAll(listPhoneDto, Phone.class);
		phoneRepository.saveAll(phones);
	}
}
