package com.moveapps.user.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.moveapps.user.dto.UserDto;
import com.moveapps.user.dto.request.UserRequest;
import com.moveapps.user.model.User;
import com.moveapps.user.repository.IUserRepository;
import com.moveapps.user.service.IPhoneService;
import com.moveapps.user.service.IUserService;
import com.moveapps.user.utils.Constants;
import com.moveapps.user.utils.Mapper;

@Service("UserService")
public class UserServiceImpl implements IUserService {

	@Autowired
	public IUserRepository userRepository;

	@Autowired
	public IPhoneService phoneService;

	@Override
	public UserDto validateDataUser(UserRequest userRequest) {
		User user = userRepository.findByEmailAndPassword(userRequest.getEmail(), userRequest.getPassword());
		UserDto userDto = new UserDto();

		if (user != null)
			return null;

		userDto = Mapper.map(user, UserDto.class);
		return userDto;
	}

	@Override
	public List<UserDto> listUsers(boolean isActive) {
		List<User> listUsuario = userRepository.listActive(isActive);

		if (listUsuario == null || listUsuario.isEmpty())
			return null;

		List<UserDto> listUserDto = Mapper.mapAll(listUsuario, UserDto.class);

		return listUserDto;
	}

	@Override
	public UserDto save(UserRequest userRequest, String token) {

		if (userRepository.findByEmail(userRequest.getEmail()) != null) {
			return null;
		}

		UserDto userDtoNew = new UserDto();
		userDtoNew.setEmail(userRequest.getEmail());
		userDtoNew.setPassword(userRequest.getPassword());
		userDtoNew.setActive(Constants.USERS_ACTIVE);
		userDtoNew.setCreated(new Date());
		userDtoNew.setToken(token);

		userDtoNew.setToken(userDtoNew.getToken());
		User usuario = Mapper.map(userDtoNew, User.class);
		usuario = userRepository.save(usuario);
		userDtoNew = Mapper.map(usuario, UserDto.class);

		phoneService.getPhoneBean(userRequest.getPhones(), usuario.getIdUser());

		return userDtoNew;
	}

}
