package com.moveapps.user.service;

import java.util.List;

import com.moveapps.user.dto.UserDto;
import com.moveapps.user.dto.request.UserRequest;

public interface IUserService {
	public UserDto validateDataUser(UserRequest userRequest);
	public List<UserDto> listUsers(boolean isActive);
	public UserDto save(UserRequest userRequest, String token);
}
