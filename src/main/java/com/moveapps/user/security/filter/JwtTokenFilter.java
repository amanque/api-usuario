package com.moveapps.user.security.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.moveapps.user.security.service.UserSecurityDetailsService;
import com.moveapps.user.utils.Constants;
import com.moveapps.user.utils.JwtToken;

@Component
public class JwtTokenFilter extends OncePerRequestFilter {

	@Autowired
	private JwtToken jwtToken;

	@Autowired
	private UserSecurityDetailsService userSecurityDetailsService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		String username = null;
		String token = null;

		final String authorizationHeader = request.getHeader(Constants.AUTHORIZATION);

		if (authorizationHeader != null && authorizationHeader.startsWith(Constants.BEARER)) {
			token = authorizationHeader.substring(7);
			username = jwtToken.extractUsername(token);
		}

		if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			UserDetails userDetails = this.userSecurityDetailsService.loadUserByUsername(username);
			if (jwtToken.validateToken(token, userDetails)) {
				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				usernamePasswordAuthenticationToken
						.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			}
		}
		
		filterChain.doFilter(request, response);

	}

}
