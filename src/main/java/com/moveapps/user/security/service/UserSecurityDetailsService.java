package com.moveapps.user.security.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.moveapps.user.security.request.UserAuthRequest;

@Service
public class UserSecurityDetailsService implements UserDetailsService{
	
	@Autowired
	@Qualifier("userAuthRequestBean")
	UserAuthRequest userAuthRequestBean;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return new User(userAuthRequestBean.getUser(), userAuthRequestBean.getPass(), new ArrayList<>());
	}

}
