package com.moveapps.user.security.request;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class UserAuthRequestComponent{

	@Value("${const.access.user}")
	private String USER;
	
	@Value("${const.access.pass}")
	private String PASS;
	
	@Bean("userAuthRequestBean") 
	private UserAuthRequest setUserAuthBean() {
		UserAuthRequest userAuthRequest = new UserAuthRequest();
		userAuthRequest.setUser(this.USER);
		userAuthRequest.setPass(this.PASS);
		return userAuthRequest;
	}

}
