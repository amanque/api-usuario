package com.moveapps.user.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.moveapps.user.dto.ResponseDto;
import com.moveapps.user.exephandler.GlobalException;
import com.moveapps.user.security.request.UserAuthRequest;
import com.moveapps.user.security.service.UserSecurityDetailsService;
import com.moveapps.user.utils.Constants;
import com.moveapps.user.utils.JwtToken;
import com.moveapps.user.utils.Util;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class AuthenticationController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserSecurityDetailsService userSecurityDetailsService;

	@Autowired
	JwtToken jwtToken;

	@ApiOperation(value = "Endpoint que devolverá un token que servirá para realizar operaciones con el resto de los endpoints.", notes = "En caso de éxito, retorne el token; En caso de error, retorne un error específico..", nickname = "autentificacion")
	@PostMapping(value = "/authentication", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({ @ApiResponse(code = 204, message = "Hubo problemas al obtener token.", response = HttpStatus.class),
			@ApiResponse(code = 200, message = "Token obtenido correctamente.", response = ResponseDto.class),
			@ApiResponse(code = 200, message = "Token obtenido correctamente.", response = ResponseDto.class) })
	public ResponseEntity<ResponseDto<String>> autentificacion(@RequestBody UserAuthRequest userAuthRequest) {

		ResponseDto<String> response = null;
		HttpStatus status = HttpStatus.OK;

		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(userAuthRequest.getUser(), userAuthRequest.getPass()));
		} catch (BadCredentialsException e) {
			status = HttpStatus.UNAUTHORIZED;
			throw new GlobalException(Constants.ERROR_USER_TOKEN, status);
		}

		final UserDetails userDetails = userSecurityDetailsService.loadUserByUsername(userAuthRequest.getUser());

		final String tokenJwt = jwtToken.generateToken(userDetails);

		response = Util.setReponse(tokenJwt);

		return new ResponseEntity<ResponseDto<String>>(response, status);
	}
}
