package com.moveapps.user.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.moveapps.user.dto.ResponseDto;
import com.moveapps.user.dto.UserDto;
import com.moveapps.user.dto.request.UserRequest;
import com.moveapps.user.exephandler.GlobalException;
import com.moveapps.user.service.IUserService;
import com.moveapps.user.utils.Constants;
import com.moveapps.user.utils.Util;
import com.moveapps.user.utils.ValidateFormat;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	IUserService userService;

	@ApiOperation(value = "Endpoint que recibe un usuario con los campos email, password, más un listado de objetos phones.", notes = "Retorna datos del usuario.", nickname = "registro")
	@PostMapping(value = "/registro", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({
			@ApiResponse(code = 204, message = "Hubo problemas al obtener datos del usuario.", response = HttpStatus.class),
			@ApiResponse(code = 200, message = "Datos del usuario obtenidos correctamente.", response = ResponseDto.class),
			@ApiResponse(code = 400, message = "Hubo problemas con formato de campos.", response = ResponseDto.class),
			@ApiResponse(code = 409, message = "Error en el servicio api rest El correo ya registrado.", response = ResponseDto.class) })
	public ResponseEntity<ResponseDto<UserDto>> registro(@RequestBody UserRequest userRequest,
			@RequestHeader(name = "Authorization") String token) {

		ResponseDto<UserDto> response = null;
		HttpStatus status = HttpStatus.OK;

		String expReg = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

		if (!ValidateFormat.valExpresionReg(expReg, userRequest.getEmail())) {
			status = HttpStatus.BAD_REQUEST;
			response = Util.setError(null, Constants.ERROR_FORMAT_EMAIL);
			throw new GlobalException(Constants.ERROR_FORMAT_EMAIL, status);
		}

		this.getValidateCharacterPass("[A-Z]", userRequest.getPassword(), Constants.CAPITAL_LETTER, Constants.EQUALS, HttpStatus.BAD_REQUEST);
		this.getValidateCharacterPass("[0-9]", userRequest.getPassword(), Constants.NUMBERS, Constants.EQUALS, HttpStatus.BAD_REQUEST);
		this.getValidateCharacterPass("[a-z]", userRequest.getPassword(), Constants.LOWER_CASE, Constants.MAYOR, HttpStatus.BAD_REQUEST);

		UserDto userDto = userService.save(userRequest, token.substring(7, token.length()));
		if (userDto == null) {
			status = HttpStatus.CONFLICT;
			response = Util.setError(null, Constants.ERROR_EMAIL_EXIST);
			throw new GlobalException(Constants.ERROR_EMAIL_EXIST, status);
		}

		response = Util.setReponse(userDto);

		return new ResponseEntity<ResponseDto<UserDto>>(response, status);
	}

	private void getValidateCharacterPass(String expReg, String password, int typeVal, int operator, HttpStatus status) {
		if (!ValidateFormat.valExpresionReg(expReg, password, typeVal, operator)) {
			throw new GlobalException(Constants.ERROR_FORMAT_EMAIL, status);
		}
	}

	@ApiOperation(value = "Endpoint devuelve todos los usuarios que se encuentran activos en el sistema (isActive = true).", notes = "En caso de éxito, retornar usuarios; En caso de error, retorne un error específico.", nickname = "lista")
	@GetMapping(value = "/lista", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiResponses({
			@ApiResponse(code = 204, message = "Hubo problemas al obtener datos del usuario.", response = HttpStatus.class),
			@ApiResponse(code = 200, message = "Datos del usuario obtenidos correctamente.", response = ResponseDto.class) })
	public ResponseEntity<ResponseDto<List<UserDto>>> lista() {
		ResponseDto<List<UserDto>> response = null;

		List<UserDto> listUserDto = userService.listUsers(Constants.USERS_ACTIVE);
		HttpStatus status = HttpStatus.OK;

		if (listUserDto == null || listUserDto.isEmpty()) {
			status = HttpStatus.NO_CONTENT;
			response = Util.setError(null, Constants.ERROR_LIST_USERS);
			throw new GlobalException(Constants.ERROR_LIST_USERS, status);
		}

		response = Util.setReponse(listUserDto);

		return new ResponseEntity<ResponseDto<List<UserDto>>>(response, status);
	}
}
